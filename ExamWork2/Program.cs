﻿/*
 * 1. Съставете програма за манипулиране на месечни данни за изработени часове по проекти на служители на софтуерна компания. За целта направете два метода (подпрограми):
 * а) за въвеждане на часовете, които даден служител е изработил за всеки ден в едномерен масив от цели неотрицателни числа;
 * б) за изчисляване на средното количество изработени часове в месеца.
 * 2. В главния метод въведете цяло число n≤31 (брой работни дни в месеца) и три едномерни масива A[n], B[n] и C[n], моделиращи изработените часове за дадения месец от трима различни служители на компанията. За всеки служител изчислете и отпечатайте стойността на средното количество изработени часове в месеца.
 * 3. За всеки служител намерете и отпечатайте номерата на дните, в които е работил над средното количество часове за месеца.
 */

namespace ExamWork2
{
    using System;
    using System.Linq;

    class Program
    {
        static int[] EnterData(int[] month, int number)
        {
            for (int x = 0; x < month.Length; x++)
            {
                Console.Write("Моля въведете часове за ден {0} на служител {1}: ", x+1, number);
                month[x] = Int32.Parse(Console.ReadLine());
            }

            return month;
        }

        static double AverageLoad(int[] month)
        {
            return Math.Round(month.Average(), 2);
        }

        static void AboveAverage(int[] month)
        {
            double avg = AverageLoad(month);

            for (int x = 0; x < month.Length; x++)
            {
                if (month[x] >= avg)
                {
                    Console.WriteLine("Отработените часове за ден {0} ({1}) са над средната стойност ({2}) за месеца.", x+1, month[x], avg);
                }
            }
            
        }

        static void Main(string[] args)
        {
            int days;

            Console.Write("Въведете брой дни в месеца: ");
            days = Int32.Parse(Console.ReadLine());

            do
            {
                Console.Clear();
                Console.Write("Въведете брой дни в месеца: ");
                days = Int32.Parse(Console.ReadLine());
            }
            while ((days < 28) || (days > 31));

            int[] A = new int[days];
            int[] B = new int[days];
            int[] C = new int[days];

            Console.Clear();
            EnterData(A, 1);
            EnterData(B, 2);
            EnterData(C, 3);

            Console.WriteLine("Средната стойност на отработени часове за служител {0} e: {1}", 1, AverageLoad(A));
            Console.WriteLine("Средната стойност на отработени часове за служител {0} e: {1}", 2, AverageLoad(B));
            Console.WriteLine("Средната стойност на отработени часове за служител {0} e: {1}", 3, AverageLoad(C));

            Console.WriteLine();
            Console.WriteLine("Дни с изработени часове над средната стойност за месеца на служител 1:");
            AboveAverage(A);
            Console.WriteLine();
            Console.WriteLine("Дни с изработени часове над средната стойност за месеца на служител 2:");
            AboveAverage(B);
            Console.WriteLine();
            Console.WriteLine("Дни с изработени часове над средната стойност за месеца на служител 3:");
            AboveAverage(C);
        }
    }
}
